#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(const Item& newItem);//add item to the set
	void removeItem(const Item&);//remove item from the set
	void ToString() const;

	//get and set functions
	void setName(string);
	string getName() const; 
	set<Item>& getSet(); 

private:
	std::string _name;
	std::set<Item> _items;




};
