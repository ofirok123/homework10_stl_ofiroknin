#pragma once

#include <iostream>
#include <string>

using namespace std;
class Item
{
public:
	Item(std::string, std::string, double);
	Item();
	~Item();
	double totalPrice() const;

	//setters
	void setName(string name);
	void setSerilNumber(string serialNumber);
	void setUnitPrice(double unitPrice);
	void setCount(int count);

	//getters
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;
	void ToString() const;
	void IncreaseCounter();
	

	//operators
	bool operator<(const Item& other) const; //compares the _serialNumber of those items.
	bool operator>(const Item& other) const; //compares the _serialNumber of those items.
	bool operator==(const Item& other) const; //compares the _serialNumber of those items.

private:
	string _name;
	string _serialNumber;
	int _count; //default is 1, can never be less than 1
	double _unitPrice;

	


	
	

	



};

