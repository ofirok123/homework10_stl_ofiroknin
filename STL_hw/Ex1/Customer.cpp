#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
	this->_items = std::set<Item>();
}

Customer::Customer()
{

}

set<Item>& Customer::getSet()
{
	return this->_items;
}

string Customer::getName() const
{
	return _name;
}

double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;
	for (it = _items.begin(); it != _items.end(); ++it)
	{
		sum += (*it).totalPrice();
	}
	return sum;
}

void Customer::addItem(const Item& newItem)
{
	this->_items.insert(newItem);
	
}

void Customer::setName(string name)
{
	_name = name;
}

void Customer::removeItem(const Item& removeItem)
{
	_items.erase(removeItem);

}

void Customer::ToString() const
{
	std::set<Item> ::iterator it;

	for (it = _items.begin(); it != _items.end(); ++it)
	{
		cout << "P name: " << (*it).getName() << endl << "Serial num:" << (*it).getSerialNumber() << endl << "Price:" << (*it).getUnitPrice() << endl << "Count:" << (*it).getCount() << "\n" << endl;
	}
}
