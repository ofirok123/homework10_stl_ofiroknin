#include "mainFunctions.h"

std::pair<string, double> findHighestPayer(std::map<std::string, Customer>& abcCustomers)
{
	std::map<string, Customer> ::iterator it;
	std::pair<string, double> maxCustomerPair;
	it = abcCustomers.begin();
	maxCustomerPair.first = (*it).first;
	maxCustomerPair.second = (*it).second.totalSum();

	for (it = ++it; it != abcCustomers.end(); ++it)
	{
		if ((*it).second.totalSum() > maxCustomerPair.second)
		{
			maxCustomerPair.first = (*it).second.getName();
			maxCustomerPair.second = (*it).second.totalSum();
		}
	}

	return maxCustomerPair;
}

void removeItems(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size)
{
	std::set<Item> ::iterator setIterator;
	Item tempItem = Item();
	Customer newCust = Customer(customerName);
	int product, i = 0;
	pair<int, bool> removeList[LIST_LEN]; //Pair.first -> count number of items, Pair.second -> tells me whether to remove the Item or not.
	printShoppingList(shoppingList, size);
	do
	{

		cout << "What item would you like to Remove from your shopping list? Input: ";
		cin >> product;
		if (product == 0)
		{
			cout << "Have fun shopping here :)\n" << endl;
			break;
		}
		else if (product > 10 || product < 0)
		{
			cout << "Invalid Item" << endl;
			break;
		}
		else
		{

			removeList[product - 1].first++;
		}
	} while (product != 0);

	for (setIterator = abcCustomers[customerName].getSet().begin(), i = 0; i < size; i++)
	{
		if (setIterator != abcCustomers[customerName].getSet().end())
		{
			if ((*setIterator).getCount() - removeList[i].first == 0)
			{
				removeList[i].first = 0;
				removeList[i].second = true;
			}
			else if ((*setIterator).getCount() - removeList[i].first > 0)
			{
				removeList[i].first = (*setIterator).getCount() - removeList[i].first;
			}
			++setIterator;
		}
		else
		{
			break;
		}
	}
	i = 0;

	for (i = 0; i != size; i++)
	{
		if (removeList[i].second == true)
		{
			abcCustomers[customerName].removeItem(shoppingList[i]);
		}
		else if (removeList[i].first > 0 && removeList[i].second == false)
		{
			tempItem = shoppingList[i];
			tempItem.setCount(removeList[i].first);
			newCust.addItem(tempItem);
		}
		else
		{
			break;
		}
	}
	reAddCustomer(newCust, abcCustomers, customerName);
}

void printMenu()
{
	cout << "Welcome to MagshiMart!\n1. to sign as customer and buy items\n2. to uptade existing customers items\n3. to print the customer who pays the most\n4. to exit" << endl;
}

void option2Menu()
{
	cout << "1.\tAdd Items\n2.\tRemove items\n3.\tBack to menu\n4.\tPrint Shopping list" << endl;
}
void printShoppingList(Item shoppinglist[], int size)
{
	cout << "The items you can buy are: (0 to exit)" << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "price: " << shoppinglist[i].getUnitPrice() << "  " << i + 1 << ". " << shoppinglist[i].getName() << endl;
	}
}

void createTheCustomer(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size, unsigned int counterArr[])
{
	Item tempItem;
	Customer cust = Customer(customerName);
	unsigned int current_count = 0;
	std::pair<string, Customer> PAIR;
	for (int i = 0; i < size; i++)
	{
		current_count = counterArr[i];
		if (current_count != 0)
		{
			tempItem = shoppingList[i];
			tempItem.setCount(current_count);
			cust.addItem(tempItem);
		}
	}
	PAIR.first = customerName;
	PAIR.second = cust;
	abcCustomers.insert(PAIR);
}

void addNewCustomer(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size, int caseNumber)
{
	int productChoice = 0;
	unsigned int createdItems[LIST_LEN] = { 0 };
	printShoppingList(shoppingList, size);
	do
	{

		cout << "What item would you like to buy? Input: ";
		cin >> productChoice;
		if (productChoice == 0)
		{
			cout << "Have fun shopping here :)\n" << endl;
			break;
		}
		else if (productChoice > 10 || productChoice < 0)
		{
			cout << "Invalid Item" << endl;
			break;
		}
		else
		{
			createdItems[productChoice - 1]++;
		}

	} while (productChoice != 0);
	if (caseNumber == 1)
	{
		createTheCustomer(abcCustomers, customerName, shoppingList, size, createdItems);
	}


}

void reAddCustomer(Customer cust, std::map<string, Customer>& abcCustomers, string custName)
{
	abcCustomers.erase(custName);
	std::pair<string, Customer> PAIR;
	PAIR.first = custName;
	PAIR.second = cust;
	abcCustomers.insert(PAIR);
}

bool in_map(std::map<string, Customer>& abcCustomers, string customerName)
{
	std::map<string, Customer> ::iterator it;
	it = abcCustomers.find(customerName);
	if (it == abcCustomers.end())
	{
		return false;
	}
	return true;
}

void addItems(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size)
{

	std::set<Item> ::iterator setIterator;
	Customer cust = Customer(customerName);
	int product, i = 0;
	Item newItem = Item();
	unsigned int counterList[LIST_LEN] = { 0 };
	printShoppingList(shoppingList, size);
	do
	{

		cout << "What item would you like to buy? Input: ";
		cin >> product;

		if (product == 0)
		{
			cout << "Have fun shopping here :)\n" << endl;
			break;
		}
		else if (product > 10 || product < 0)
		{
			cout << "Invalid Item" << endl;
			break;
		}
		else
		{
			counterList[product - 1]++;
		}

	} while (product != 0);

	setIterator = abcCustomers[customerName].getSet().begin();

	for (i = 0; i < size; i++)
	{
		if (setIterator != abcCustomers[customerName].getSet().end())
		{
			counterList[i] += (*setIterator).getCount();
			++setIterator;
		}

	}

	i = 0;
	while (i != size)
	{
		if (counterList[i] != 0)
		{
			newItem = shoppingList[i];
			newItem.setCount(counterList[i]);
			cust.addItem(newItem);
		}
		i++;
	}
	reAddCustomer(cust, abcCustomers, customerName);
}
