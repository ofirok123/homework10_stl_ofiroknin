#pragma once
#include "Customer.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <utility>
#include "Item.h"
#include<map>
#define LIST_LEN 10


//print functions
void printMenu();
void option2Menu();
void printShoppingList(Item shoppinglist[], int size);

//helper functions

void createTheCustomer(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size, unsigned int counterArr[]);
bool in_map(std::map<string, Customer>& abcCustomers, string customerName);
void reAddCustomer(Customer cust, std::map<string, Customer>& abcCustomers, string custName);

//Main functions
void addNewCustomer(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size, int caseNumber);
void addItems(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size);
void removeItems(std::map<string, Customer>& abcCustomers, string customerName, Item shoppingList[], int size);
std::pair<string, double> findHighestPayer(std::map<std::string, Customer>& abcCustomers);
