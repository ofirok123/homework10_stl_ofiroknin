#include "Item.h"
#define COUNT_ERROR "Count can be under 1"
#define PRICE_ERROR "Unit price can't be under 0"


Item::Item(string name, string serialNumber,double unitPrice)
{
	setName(name);
	this->_count = 1;
	setSerilNumber(serialNumber);
	setUnitPrice(unitPrice);
}

Item::Item()
{

}

void Item::IncreaseCounter() 
{
	this->_count++;
}

Item::~Item()
{

}

void Item::setName(string name) 
{
	this->_name = name;
}

void Item::setSerilNumber(string serialNumber)
{
	try
	{
		if (serialNumber.length() != 5)
		{
			throw "Serial number must be 5 numbers";
		}
		this->_serialNumber = serialNumber;
	}
	catch (const char* str)
	{
		std::cerr << str << endl;
		_exit(1);
	}
	
}

void Item::setCount(int count)
{
	this->_count = count;
	
}

void Item::setUnitPrice(double unitPrice)
{
	try
	{
		if (unitPrice <= 0)
		{
			throw PRICE_ERROR;
		}
		this->_unitPrice = unitPrice;
	}
	catch (const char* str)
	{
		cerr << str << endl;
		_exit(1);
	}
}

string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;

}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;

}

bool Item::operator<(const Item& other) const
{
	return (this->_serialNumber < other._serialNumber);
}

bool Item::operator>(const Item& other) const
{
	return (this->_serialNumber > other._serialNumber);
}

bool Item::operator==(const Item& other) const
{
	return (this->_serialNumber == other._serialNumber);
}


void Item::ToString() const
{
	cout << "product name: " << _name << "   price:" << _unitPrice << "  serial num: " << _serialNumber;
}