#include "mainFunctions.h"
#include "Customer.h"
#include <iostream>
#include "Item.h"
#include<map>
#define LIST_LEN 10
#define SIGN_NEW_CUSTOMER 1
#define UPDATE_CUSTOMER_LIST 2
#define MOST_PAID 3
#define EXIT 4
#define ADD_ITEMS 1
#define REMOVE_ITEMS 2
#define BACK_TO_MENU 3
#define PRINT_SHOPPING_LIST 4



int main()
{
	std::map<std::string, Customer> abcCustomers;
	std::map<string, Customer> ::iterator it;
	Item itemList[LIST_LEN] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	const unsigned int itemListLength = end(itemList) - begin(itemList);
	int choice = 0;
	int option2Choice = 0;
	string customerName = "";
	
	do
	{
		printMenu();
		cin >> choice;

		switch (choice)
		{
		case SIGN_NEW_CUSTOMER: // sign a new customer
			cout << "Please enter your name: ";
			cin >> customerName;
			
			if (!in_map(abcCustomers,customerName)) // new customer
			{
				addNewCustomer(abcCustomers, customerName, itemList, itemListLength,1);
			}
			else
			{
				cerr << "Name already in the map!" << endl;
			}
			break;
		case UPDATE_CUSTOMER_LIST: //update list of customer
			
			cout << "Please identify by your name:";
			cin >> customerName;
			if (in_map(abcCustomers, customerName))
			{
				option2Menu();
				cin >> option2Choice;
				if (option2Choice == ADD_ITEMS) //add items
				{
					addItems(abcCustomers, customerName, itemList, itemListLength);
				}
				else if(option2Choice == REMOVE_ITEMS)
				{
					removeItems(abcCustomers,customerName,itemList,itemListLength);
				}
				else if (option2Choice == BACK_TO_MENU)
				{
					break;
				}
				else if(option2Choice == PRINT_SHOPPING_LIST)
				{
					abcCustomers[customerName].ToString();
				}
			}
			else
			{
				cout << "You are not in our list!" << endl;
			}
		
			break;
		case MOST_PAID: // most paid
			cout << "Highest payer Customer:" << findHighestPayer(abcCustomers).first << endl;
			cout << "His total Price:" << findHighestPayer(abcCustomers).second << "\n" << endl;

			break;

		case EXIT:
			cout << "Bye\n";
			_exit(1);
			break;

		default:
			cout << "Invalid choice, only between 1 - 4\n" << endl;
			break;
		}

	} while (choice != 4);

	return 0;
}



